<?php
/**
 * Fired when the plugin is uninstalled.
 *
 * @package   WP_Collab
 * @author    DevriX
 * @license   GPL-2.0+
 * @link      http://devrix.com
 * @copyright 2016 DevriX
 */

// If uninstall not called from WordPress, then exit
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

// uninstall functionality here
