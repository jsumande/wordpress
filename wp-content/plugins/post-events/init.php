<?php
/**
 * Plugin Name: Post Events
 * Plugin URI: http://test.com
 * Description: Plugin Description
 * Version: 1.0
 * Author: Jayvee Sumande
 * Author URI: http://testevents.com
 */
 

/*INCLUDE FILES*/

	require_once(ABSPATH."wp-includes/pluggable.php");




function my_plugin_activate() {


}
register_activation_hook(   __FILE__, 'my_plugin_activate' );


	//Register Custom Post Type
	function custom_post_type() {

		$labels = array(
			'name'                  => _x( 'Post Events', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'Post Event', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'Post Events', 'text_domain' ),

		);
		$args = array(
			"label" => __( "Post Events", "twentyseventeen" ),
			"labels" => $labels,
			"description" => "Post Event Description",
			"public" => true,
			"publicly_queryable" => true,
			"show_ui" => true,
			"show_in_rest" => false,
			"rest_base" => "",
			"has_archive" => false,
			"show_in_menu" => true,
			"exclude_from_search" => false,
			"capability_type" => "post",
			"map_meta_cap" => true,
			"hierarchical" => false,
			"rewrite" => array( "slug" => "post_events", "with_front" => true ),
			"query_var" => true,
			"supports" => array( "title",'value'),
		);
		register_post_type( 'post_events', $args );

	}
	add_action( 'init', 'custom_post_type', 0 );

	// if(function_exists("register_field_group"))
	// {
	// 	register_field_group(array (
	// 		'id' => 'acf_post-events',
	// 		'title' => 'Post Events',
	// 		'fields' => array (
	// 			array (
	// 				'key' => 'field_59e47dd19580c',
	// 				'label' => 'Date',
	// 				'name' => 'date',
	// 				'type' => 'date_picker',
	// 				'required' => 1,
	// 				'date_format' => 'yymmdd',
	// 				'display_format' => 'dd/mm/yy',
	// 				'first_day' => 1,
	// 			),
	// 			array (
	// 				'key' => 'field_59e47dbc9580b',
	// 				'label' => 'Location',
	// 				'name' => 'location',
	// 				'type' => 'text',
	// 				'default_value' => '',
	// 				'placeholder' => '',
	// 				'prepend' => '',
	// 				'append' => '',
	// 				'formatting' => 'html',
	// 				'maxlength' => '',
	// 			),
	// 			array (
	// 				'key' => 'field_59e47de89580d',
	// 				'label' => 'URL',
	// 				'name' => 'url',
	// 				'type' => 'text',
	// 				'required' => 1,
	// 				'default_value' => '',
	// 				'placeholder' => '',
	// 				'prepend' => '',
	// 				'append' => '',
	// 				'formatting' => 'html',
	// 				'maxlength' => '',
	// 			),
	// 		),
	// 		'location' => array (
	// 			array (
	// 				array (
	// 					'param' => 'post_type',
	// 					'operator' => '==',
	// 					'value' => 'post_events',
	// 					'order_no' => 0,
	// 					'group_no' => 0,
	// 				),
	// 			),
	// 		),
	// 		'options' => array (
	// 			'position' => 'normal',
	// 			'layout' => 'no_box',
	// 			'hide_on_screen' => array (
	// 				0 => 'permalink',
	// 				1 => 'the_content',
	// 				2 => 'excerpt',
	// 				3 => 'custom_fields',
	// 				4 => 'discussion',
	// 				5 => 'comments',
	// 				6 => 'revisions',
	// 				7 => 'slug',
	// 				8 => 'author',
	// 				9 => 'format',
	// 				10 => 'featured_image',
	// 				11 => 'categories',
	// 				12 => 'tags',
	// 				13 => 'send-trackbacks',
	// 			),
	// 		),
	// 		'menu_order' => 0,
	// 	));
	// }

function display_custom_post_events() {
// args
$args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'post_events'
);


// query
$the_query = new WP_Query( $args );
?>
<?php if( $the_query->have_posts() ): ?>
	<?php
		$date = date_create(get_field('date'));
	?>
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<h3><?php echo get_the_title(); ?></h3>
		<li>Event Date: 
			<?php echo date_format($date,'M d, Y'); ?>
		</li>
		<li>Location:
			<a target="_blank"href="http://maps.google.com/?q=<?php echo  urlencode(get_field('location')); ?>" >
			<?php echo get_field('location') ?>
			</a>
		</li><li>Url: 
			<?php echo get_field('url') ?>
		</li>
		<li>
			<a href="https://www.google.com/calendar/render?action=TEMPLATE&text=<?php echo get_the_title(); ?>&dates=<?php echo date_format($date,'Ymd'); ?>/<?php echo date_format($date,'Ymd'); ?>&details=aa&location=<?php echo get_field('location') ?>&sf=true&output=xml"
			target="_blank" rel="nofollow">Add to my calendar</a>			
		</li>
	<?php endwhile; ?>
<?php endif; 
}
add_shortcode( 'custom_post_events', 'display_custom_post_events' );

function custom_post_events_metabox() {
   add_meta_box(
       'custom_post_events_date',       // $id
       'Date',                  // $title
       'show_custom_post_events_date',  // $callback
       'post_events',                 // $page
       'normal',                  // $context
       'high'                     // $priority
   );
}
add_action('add_meta_boxes', 'custom_post_events_metabox');

function show_custom_post_events_date() {
    global $post;

    // Use nonce for verification to secure data sending
    wp_nonce_field( basename( __FILE__ ), 'wpse_our_nonce' );

    ?>

    <!-- my custom value input -->
    <input type="number" name="wpse_value" value="">

    <?php
}
?>

