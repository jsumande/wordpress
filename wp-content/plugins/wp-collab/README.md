# WP Collab #
Contributors: devrix

Tags: project, management, pm, collaborative, project management, wpcollab, wp-collab, collab 

Requires at least: 3.5.1

Tested up to: 3.8.1

Stable tag: 1.0.0

License: GPLv2 or later

License URI: http://www.gnu.org/licenses/gpl-2.0.html

WordPress plugin that makes project management easier.

##  Description 
WordPress plugin that makes project management easier.
Good choice for freelancers, developers, designers, small teams and everyone who looking for simple project management tool.

## Getting Started

Installation is straightforward using the usual WordPress Plugins -> Add New procedure.

- Within WordPress Dashboard, click `Plugins` -> `Add New`
- Click the `Upload` button and select the ZIP file you just downloaded.
- Click the `Install` button

### Change Log
-----------------------------------------------------------------------------------------

###### Version 1.1.0
New: Add custom statuses
New: Add taxonomy for project types
New: Proper translation strings and .POT file ready for translation
New: New option for removing password checkpoint if the current user is admin
New: New option for adding project contributor
New: New option for adding/displaying project manager and contributor avatar
