(function ( $ ) {
    "use strict";

    $(function () {

        $( document ).keypress( stopEnterSubmission );

        handleTags();

        var tagsInput = $( '.bootstrap-tagsinput input' );

        tagsInput.keydown(function ( e ) {
            var tagsLength = $( '.label-info' ).length;
            var key = e.keyCode || e.charCode;
            if ( tagsInput.val() === '' && tagsLength === 1 && key === 8 || tagsInput.val() === '' && tagsLength === 1 && key === 46 ) {
                e.preventDefault();
                e.stopPropagation();
            }
        });

        $( '.bootstrap-tagsinput' ).on( 'click keydown',  function ( e ) {
            handleTags();
        } );

        tagsInput.blur( function () {
            handleTags();
        } );

        function handleTags() {
            if( $( '.label-info' ).length === 1 ) {
                $( 'span[data-role="remove"]' ).hide();
            }else {
                $( 'span[data-role="remove"]' ).show();
            }
        }

        function stopEnterSubmission( evt ) {
            var node = ( evt.target ) ? evt.target : ( ( evt.srcElement ) ? evt.srcElement : null );
            if ( ( evt.keyCode === 13 ) && ( node.type === "text" ) ) {
                return false;
            }
        }

    });

}(jQuery));