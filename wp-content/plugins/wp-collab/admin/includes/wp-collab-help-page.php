<div class="dx-help-page">
	<header class="help-header">
		<h2 class="logo"><img
				src="<?php echo plugins_url( '../assets/img/help-page/devrix-logo-white.svg', __FILE__ ); ?>"
				alt=""/></h2><!-- .logo -->
		<div class="page-info">
			<h3 class="page-title"><?php echo __( 'Documentation', 'wp-collab' ) ?></h3><!-- .page-title -->
			<h4 class="page-subtitle"><?php echo __( 'WP Collab', 'wp-collab' ) ?></h4><!-- .page-subtitlr -->
		</div><!-- .info -->
	</header><!-- .help-header -->

	<section class="section-primary">

		<div class="panel-content">
			<header class="help-heading">
				<h1 class="help-title"><?php echo __( 'Welcome to WP Collab help page', 'wp-collab' ) ?></h1>
				<!-- .help-title -->
				<p class="help-subtitle"><?php echo __( 'WP Collab is a WordPress plugin that makes project management easier.', 'wp-collab' ) ?></p>
				<!-- .help-subtitle -->
			</header><!-- .help-heading -->

			<div class="main-content">

				<section id="overview">

					<h2>
						<?php echo __( 'Overview', 'wp-collab' ) ?>
					</h2>
					<br>

					<p>
						<?php echo __( 'WP-Collab plugin allows you to create nice and friendly clientarea. You can keep project information right on your WordPress website. Also you can discuss projects with your clients and colleagues.', 'wp-collab' ) ?>
					</p>
					<br>

					<p>
						<?php echo __( 'We developed this plugin because we didn\'t find suitable solution for simple project management. Most of existing PM software is either overloaded or has unsuitable price. 
                                        But the main disadvantage of most PM software is that your client should be registered to be able to discuss project/view current status. 
                                        So we decided to develop the plugin with the help of which you can share project with your clients and colleagues using URL with access key. And anyone who has this URL is able to view project status and leave a comment. There is no headache with client registration!', 'wp-collab' ) ?>
					</p>
					<br>


					<p>
						<?php echo __( 'WP-Collab plugin creates password protected posts with special UID and access URL, and anyone who have access URL (or project UID and password) can view project and leave a comment (if comments are allowed).', 'wp-collab' ) ?>
					</p>
					<h4>
						<?php echo __( 'Easy, useful and friendly!', 'wp-collab' ) ?>
					</h4>
				</section>

				<section id="getting-started">
					<h2><?php echo __( '1. Getting Started', 'wp-collab' ) ?></h2>
					<br>

					<article id="installation">
						<h3>
							<?php echo __( '1.1 Installation', 'wp-collab' ) ?>
						</h3>
						<br>

						<p>
							<?php echo __( 'At first you need to upload WP Collab plugin to your WordPress installtion. You can upload this plugin manually via FTP or WordPress admin panel.', 'wp-collab' ) ?>
						</p>
						<br>

						<h3>
							<?php echo __( 'Adding Plugin Manually via FTP', 'wp-collab' ) ?>
						</h3>
						<br>

						<p>
							<?php echo __( 'Use your FTP Client to upload non-zipped folder called "wp-collab" (it is located inside of the main zip package) into the "/wp-content/plugins/" folder on your server.', 'wp-collab' ) ?>
						</p>


						<h3>
							<?php echo __( 'Adding Plugin Using the WordPress Admin', 'wp-collab' ) ?>
						</h3>
						<br>

						<p>
							<?php echo __( 'To add a new plugin to your WordPress installation, follow these basic steps:', 'wp-collab' ) ?>
						</p>
						<ol>
							<li>
								<?php echo __( 'Log in to the WordPress Administration', 'wp-collab' ) ?>
							</li>
							<li>
								<?php echo __( 'Go to ', 'wp-collab' ) ?>
								<strong><?php echo __( '"Plugins > Add New > Upload"', 'wp-collab' ) ?></strong></li>
							<li>
								<?php echo __( 'Click on ', 'wp-collab' ) ?>
								<strong><?php echo __( '"Choose File" ', 'wp-collab' ) ?></strong>
								<?php echo __( 'and select the ZIP file', 'wp-collab' ) ?>
								<strong><?php echo __( '"wp-collab.zip" ', 'wp-collab' ) ?></strong>
								<?php echo __( '(it is located inside of the main zip package)', 'wp-collab' ) ?>
							</li>
							<li>
								<?php echo __( 'Log in to the WordPress Administration', 'wp-collab' ) ?>
							</li>
						</ol>
						<img src="<?php echo plugins_url( '../assets/img/help-page/scr-1-1.png', __FILE__ ); ?>" alt="">
						<p>
							<?php echo __( 'Once the plugin is uploaded, you need to activate it. Go to ', 'wp-collab' ) ?>
							<strong><?php echo __( '"Plugins" ', 'wp-collab' ) ?></strong>
							<?php echo __( 'and activate ', 'wp-collab' ) ?>
							<strong><?php echo __( '"WP Collab" ', 'wp-collab' ) ?></strong>
							<?php echo __( 'plugin.', 'wp-collab' ) ?>
						</p>
						<img src="<?php echo plugins_url( '../assets/img/help-page/scr-1-2.png', __FILE__ ); ?>" alt="">
						<p><?php echo __( 'Congratulations, you have successfully installed WP Collab plugin!', 'wp-collab' ) ?></p>
					</article>

					<article id="plugin-settings">
						<h3>
							<?php echo __( '1.2 Plugin Settings', 'wp-collab' ) ?>
						</h3>
						<p>
							<?php echo __( 'Login to the WordPress admin panel and go to ', 'wp-collab' )?>
							<strong><?php echo __( '"Projects > Settings (1)" ', 'wp-collab' ) ?></strong>
							<?php echo __( '(it is located under ', 'wp-collab' )?>
							<strong><?php echo __( '"Comments"', 'wp-collab' ) ?></strong>
							<?php echo __( 'and above ', 'wp-collab' )?>
							<strong><?php echo __( '"Appearance"', 'wp-collab' ) ?></strong>
							<?php echo __(') You can configure user-friendly url for your client area (2) and define prefix for project identifier (UID) (3)', 'wp-collab' ) ?>
						</p>
						<img src="<?php echo plugins_url( '../assets/img/help-page/scr-2-1.png', __FILE__ ); ?>" alt="">
					</article>

					<article id="create-your-client-area">
						<h3><?php echo __( '1.3 Create and configure your client area', 'wp-collab' ) ?></h3>
						<p><?php echo __( 'At first you need to configure friendly slug-URL for your client area. 
											Go to ', 'wp-collab' ) ?>
							<strong><?php echo __( '"Projects > Settings" ', 'wp-collab' ) ?></strong>
							<?php echo __( '(it is located under ', 'wp-collab' ) ?>
							<strong><?php echo __( '"Comments" ', 'wp-collab' ) ?></strong>
							<?php echo __( 'and above', 'wp-collab' ) ?>
							<strong><?php echo __( '"Appearance"', 'wp-collab' ) ?></strong>
							<?php echo __( ') and change value for ', 'wp-collab' ) ?>
							<strong><?php echo __( '"Projects Permalink" ', 'wp-collab' ) ?></strong>
							<?php echo __( 'option to any value that you want.
											Default value for ', 'wp-collab' ) ?>
							<strong><?php echo __( '"Projects Permalink" ', 'wp-collab' ) ?></strong>
							<?php echo __( 'is ', 'wp-collab' ) ?>
							<strong><?php echo __( '"projects" ', 'wp-collab' ) ?></strong>
							<?php echo __( ', it means that your client area will be available at the following URL: ', 'wp-collab' ) ?>
							<strong><?php echo __( 'http://yourdomain.com/projects/', 'wp-collab' ) ?></strong>
						</p>
						<img src="<?php echo plugins_url( '../assets/img/help-page/scr-2-1-1.png', __FILE__ ); ?>"
						     alt="">
						<p>
							<?php echo __( 'For example, if you will change value for ', 'wp-collab' ) ?>
							<strong><?php echo __( '"Projects Permalink" ', 'wp-collab' ) ?></strong>
							<?php echo __( 'to ', 'wp-collab' ) ?>
							<strong><?php echo __( '"clientarea"', 'wp-collab' ) ?></strong>
							<?php echo __( ' then your client area will be available at the URL: ', 'wp-collab' ) ?>
							<strong><?php echo __( 'http://yourdomain.com/clientarea/', 'wp-collab' ) ?></strong>
						</p>
						<p>
							<?php echo __( 'You can add the link to your client area to the menu on your website. Go to ', 'wp-collab' ) ?>
							<strong><?php echo __( '"Appearance > Menus > Links" ', 'wp-collab' ) ?></strong>
							<?php echo __( 'and add new menu item with client area URL that you have defined in the previous step. And then click ', 'wp-collab' ) ?>
							<strong><?php echo __( '"Save".', 'wp-collab' ) ?></strong>
						</p>
						<img src="<?php echo plugins_url( '../assets/img/help-page/scr-2-2-1.png', __FILE__ ); ?>"
						     alt="">
						<h4>
							<?php echo __( 'Client area frontend', 'wp-collab' ) ?>
						</h4>
						<p>
							<?php echo __( 'The access form will be displayed if the user is not logged in (or logged in as someone who can not "edit posts", eg subscriber)', 'wp-collab' ) ?>
						</p>
						<img src="<?php echo plugins_url( '../assets/img/help-page/scr-2-2.png', __FILE__ ); ?>" alt="">
						<p>
							<?php echo __( 'The list of all projects will be shown if the user is logged in as administrator (or someone who can "edit posts" - editor/author/contributor/etc.)', 'wp-collab' ) ?>
						</p>
						<img src="<?php echo plugins_url( '../assets/img/help-page/scr-2-3.png', __FILE__ ); ?>" alt="">
					</article>

					<article id="project-settings">
						<h3>
							<?php echo __( 'Project settings', 'wp-collab' ) ?>
						</h3>
						<p>
							<?php echo __( 'You can define prefix for project UID.', 'wp-collab' ) ?>
						</p>
						<p>
							<?php echo __( 'This option is used to generate a permalink of the project. Project permalink consists of ', 'wp-collab' ) ?>
							<strong><?php echo __( '"UID Prefix" ', 'wp-collab' ) ?></strong>
							<?php echo __( 'and ', 'wp-collab' ) ?>
							<strong><?php echo __( '"post_id" ', 'wp-collab' ) ?></strong>
							<?php echo __( '(using function of the string concatenation). ', 'wp-collab' ) ?>
						</p>
						<p>
							<?php echo __( 'Default prefix is ', 'wp-collab' ) ?>
							<strong>"1000"</strong>
							<?php echo __( ', it means that the project with ', 'wp-collab' ) ?>
							<strong><?php echo __( 'post_id="16"', 'wp-collab' ) ?></strong>
							<?php echo __( 'will have the project UID (permalink)=', 'wp-collab' ) ?>
							<strong>"100016"</strong>
						</p>
						<img src="<?php echo plugins_url( '../assets/img/help-page/scr-2-1-2.png', __FILE__ ); ?>"
						     alt="">
						<p>
							<?php echo __( 'On the screenshot below project permalink is ', 'wp-collab' ) ?>
							<strong>"999-16"</strong>
							<?php echo __( ', it means that the ', 'wp-collab' ) ?>
							<strong><?php echo __( 'UID Prefix="999-" ', 'wp-collab' ) ?></strong>
							<?php echo __( 'and ', 'wp-collab' ) ?>
							<strong><?php echo __( 'post_id="16"', 'wp-collab' ) ?></strong>
						</p>
						<img src="<?php echo plugins_url( '../assets/img/help-page/scr-2-4.png', __FILE__ ); ?>" alt="">
					</article>

					<article id="adding-new-project">
						<h3>
							<?php echo __( '1.4 Adding new project', 'wp-collab' ) ?>
						</h3>
						<p>
							<?php echo __( 'Login to the WordPress admin panel and go to ', 'wp-collab' ) ?>
							<strong><?php echo __( '"Projects > Add New"', 'wp-collab' ) ?></strong>
						</p>
						<img src="<?php echo plugins_url( '../assets/img/help-page/scr-3-1.png', __FILE__ ); ?>" alt="">
						<p>
							<?php echo __( 'Notes to the screenshot:', 'wp-collab' ) ?>
						</p>
						<ol>
							<li>
								<?php echo __( 'Project title', 'wp-collab' ) ?>
							</li>
							<li>
								<?php echo __( 'Project description', 'wp-collab' ) ?>
							</li>
							<li>
								<?php echo __( 'Featured image', 'wp-collab' ) ?>
							</li>
							<li>
								<?php echo __( 'Use shortcode ', 'wp-collab' ) ?>
								<strong><?php echo __( '[wpcollab_admin_only]', 'wp-collab' ) ?></strong>
								<?php echo __( 'to hide content from your clients, this content will be visible only for someone who can "edit_posts" (admin/contributor/author/etc)', 'wp-collab' ) ?>
							</li>
							<li>
								<?php echo __( 'List of users who can "edit_posts"', 'wp-collab' ) ?>
							</li>
							<li>
								<?php echo __( 'Project status (Active/Not Active)', 'wp-collab' ) ?>
							</li>
							<li>
								<?php echo __( 'Admin notes', 'wp-collab' ) ?>
							</li>
							<li>
								<?php echo __( 'Client Details', 'wp-collab' ) ?>
							</li>
							<li>
								<?php echo __( 'Additional client information: address, skype, billing info, etc.', 'wp-collab' ) ?>
							</li>
							<li>
								<?php echo __( 'You can use additional custom fields if you want', 'wp-collab' ) ?>
							</li>
							<li>
								<?php echo __( 'You can disable/enable comments for a project if you want', 'wp-collab' ) ?>
							</li>
						</ol>
					</article>

					<article id="project-sharing">
						<h3>
							<?php echo __( '1.5 Project sharing and discussions', 'wp-collab' ) ?>
						</h3>

						<div id="backend">
							<h3>
								<?php echo __( 'Backend', 'wp-collab' ) ?>
							</h3>
							<p>
								<?php echo __( 'You can share and discuss project with your colleagues and your clients', 'wp-collab' ) ?>
							</p>
							<p>
								<?php echo __( 'You can share your project through the projects list. Go to ', 'wp-collab' ) ?>
								<strong><?php echo __( '"Projects"', 'wp-collab' ) ?></strong>
								<?php echo __( ', copy ', 'wp-collab' ) ?>
								<strong><?php echo __( '"Access URL" (1) ', 'wp-collab' ) ?></strong>
								<?php echo __( 'and share it with your client or colleague ', 'wp-collab' ) ?>
							</p>
							<img src="<?php echo plugins_url( '../assets/img/help-page/scr-3-2.png', __FILE__ ); ?>" alt="">
							<p>
								<?php echo __( 'Also you can share your project through the project edit screen. Go to ', 'wp-collab' ) ?>
								<strong><?php echo __( '"Projects" ', 'wp-collab' ) ?></strong>
								<?php echo __( ', and click ', 'wp-collab' )?>
								<strong><?php echo __( '"Edit"', 'wp-collab' ) ?></strong>
								<?php echo __( 'on the project that you want to share', 'wp-collab' ) ?>
							</p>
							<img src="<?php echo plugins_url( '../assets/img/help-page/scr-4-1.png', __FILE__ ); ?>" alt="">
							<p>
								<?php echo __( 'Notes to the screenshot:', 'wp-collab' ) ?>
							</p>
							<ol>
								<li><?php echo __( 'You can share access details (project uid and password)', 'wp-collab') ?></li>
								<li><?php echo __( 'Also you can share project using special URL with access key, anyone who have this URL can view project', 'wp-collab') ?></li>
								<li><?php echo __( 'Sample message is generated for your convenience, you can copy it and send it by email to your colleagues and clients', 'wp-collab') ?></li>
								<li><?php echo __( 'You can view and manage comments for this project', 'wp-collab') ?></li>
							</ol>
						</div>

						<div id="frontend">
							<h3>
								<?php echo __( 'Frontend', 'wp-collab' )?>
							</h3>

							<div id="admin-view">
								<h3>
									<?php echo __( 'Admin View', 'wp-collab' ) ?>
								</h3>
								<p>
									<?php echo __( 'In case, if current user can "edit_posts" (admin/contributor/author/etc) — all hidden information will be displayed (points 2,3,4) ', 'wp-collab' ) ?>
								</p>
								<img src="<?php echo plugins_url( '../assets/img/help-page/scr-4-2.png', __FILE__ ); ?>" alt="">
							</div>

							<div id="client-view">
								<h3>
									<?php echo __( 'Client View (Guest/Subscriber)', 'wp-collab' ) ?>
								</h3>
								<p>
									<?php echo __( 'In case, if current user can\'t "edit_posts" (admin/contributor/author/etc) or just isn\'t logged in — all hidden information will NOT be displayed (points 2,3)', 'wp-collab' ) ?>
								</p>
								<p>
									<?php echo __( 'If comments are allowed to anyone — guest can leave comment for the project (point 4). This option is useful for your clients.', 'wp-collab' ) ?>
								</p>
								<img src="<?php echo plugins_url( '../assets/img/help-page/scr-4-3.png', __FILE__ ); ?>" alt="">
							</div>

						</div>
					</article>

				</section>

				<section id="advanced-customization">
					<h2>
						<?php echo __( '2. Advanced Customization', 'wp-collab' )?>
					</h2>
					<div class="alert-box success">
						<?php echo __( 'Please note: this section is for advanced users and web-developers. You should be familiar with editing CSS and PHP files. ', 'wp-collab' ) ?>
						<br>
						<?php echo __( 'If you are not an advanced user or web-developer — we can help you with customization. Please ', 'wp-collab') ?>
						<a href="https://devrix.com/contact/" target="_blank">
							<strong>
								<?php echo __( 'contact us', 'wp-collab' ) ?>
							</strong>
						</a>
						<?php echo __( ' for more information.', 'wp-collab' ) ?>
					</div>
					<p>
						<?php echo __( 'You can change styles and layout for your clientarea.', 'wp-collab' ) ?>
					</p>
					<p>
						<?php echo __( 'To change styles just add new styles to ', 'wp-collab')?>
						<strong><?php echo __( '"style.css" ', 'wp-collab' ) ?></strong>
						<?php echo __( 'of your current WP-Theme', 'wp-collab' ) ?>
					</p>
					<p>
						<?php echo __( 'If you want to change layout — just copy template files ', 'wp-collab' )?>
						<strong><?php echo __( '"single-wpcollab_project.php" ', 'wp-collab' ) ?></strong>
						<?php echo __( '(single project view) and ', 'wp-collab' )?>
						<strong><?php echo __( '"archive-wpcollab_project.php"', 'wp-collab' ) ?></strong>
						<?php echo __( '(projects list) from folder ', 'wp-collab' )?>
						<strong><?php echo __( '"/wp-content/plugins/wp-collab/templates" ', 'wp-collab' ) ?></strong>
						<?php echo __( 'into your current theme folder.', 'wp-collab' ) ?>
					</p>
					<div class="alert-box warning">
						<?php echo __( 'Be aware with conditional logic', 'wp-collab' ) ?>
						<i>
							<?php echo __( 'if(current_user_can( \'edit_posts\' )){ ... }', 'wp-collab' ) ?>
						</i>
					</div>
					<p>
						<?php echo __( 'In the example below, we have changed the background color of ', 'wp-collab' )?>
						<strong><?php echo __( '"admin_only" ', 'wp-collab' ) ?></strong>
						<?php echo __( 'text (point 1) and the location of the project manager and client information (point 2) ', 'wp-collab' ) ?>
					</p>
					<img src="<?php echo plugins_url( '../assets/img/help-page/scr-5-1.png', __FILE__ ); ?>" alt="">
				</section>

				<section id="changelog">
					<h2>
						<?php echo __( '3. Changelog', 'wp-collab' ) ?>
					</h2>
					<h4>
						<?php echo __( 'Version 1.0.0 ', 'wp-collab' ) ?>
					</h4>
					<p>
						<?php echo __( '- Initial release', 'wp-collab' ) ?>
					</p>
				</section>

				<section id="thanks">
					<h2>
						<?php echo __( 'Once again, thank you so much for purchasing this item.', 'wp-collab' ) ?>
					</h2>
					<p>
						<?php echo __( 'We hope you will be happy using our plugin. If you have any questions about this plugin, don\'t hesitate to ', 'wp-collab' )?>
						<a href="https://devrix.com/contact/" target="_blank">
							<?php echo __( 'contact us', 'wp-collab' )?>
						</a>
						<?php echo __(', we will do our best to please you.', 'wp-collab' ) ?>

					</p>
				</section>

				<div class="dx-help-footer">
					<h3><?php echo __( 'And much more', 'wp-collab' ) ?></h3>
					<p><?php echo __( 'Follow us on ', 'wp-collab' ) ?><a href="https://twitter.com/wpdevrix"
					                                                      target="_blank"><?php echo __( 'Twitter', 'wp-collab' ) ?></a> <?php echo __( 'and', 'wp-collab' ) ?>
						<a href="https://www.facebook.com/DevriXShop/"
						   target="_blank"><?php echo __( 'Facebook', 'wp-collab' ) ?></a></p>
					<footer class='dx-footer'>
						<a href="http://devrix.com/shop/subscribe/" target="_blank">
							<img class='footer-banner'
							     src="<?php echo WPScraper::get_instance()->get_plugin_url() . '/images/dx-help-banner.png'; ?>"
							     alt="WordPress help">
						</a>
					</footer>
				</div><!-- .dx-help-footer -->
				
			</div><!-- .main-content -->

		</div><!-- .panel-content -->

		<aside class="panel-sidebar">
			<h2 class="widget-heading"><?php echo __( 'Other DevriX Plugins', 'wp-collab' ) ?></h2>
			<!-- .widget-heading -->
			<div class="plugins-list">
				<a class="item" href="http://devrix.com/shop/downloads/edd-product-updates/" target="_blank">
					<img
						src="<?php echo WPScraper::get_instance()->get_plugin_url() . '/images/product-upates-featuerd-image-150x150.png'; ?>"
						alt=""/>
					<div class="content">
						<strong><?php echo __( 'EDD Product Updates', 'wp-collab' ) ?></strong>
						<p class="plugin-desc"><?php echo __( 'Send personalized email messages to your customers.', 'wp-collab' ) ?></p>
					</div><!-- .content -->
				</a>

				<a class="item" href="http://devrix.com/shop/downloads/wpscraper/" target="_blank">
					<img
						src="<?php echo WPScraper::get_instance()->get_plugin_url() . '/images/wp-scraper-banner-150x150.jpg'; ?>"
						alt=""/>
					<div class="content">
						<strong><?php echo __( 'WPScrapper', 'wp-collab' ) ?></strong>
						<p class="plugin-desc"><?php echo __( 'WPScraper is a WordPress plugin that allows you to fetch or scrap data from other websites', 'wp-collab' ) ?></p>
					</div><!-- .content -->
				</a>

				<a class="item" href="http://devrix.com/shop/downloads/wpbounce-anti-bounce-wordpress-plugin/"
				   target="_blank">
					<img
						src="<?php echo WPScraper::get_instance()->get_plugin_url() . '/images/wpbounce-banner-150x150.jpg'; ?>"
						alt=""/>
					<div class="content">
						<strong><?php echo __( 'WPBounce – Anti Bounce WordPress Plugin', 'wp-collab' ) ?></strong>
						<p class="plugin-desc"><?php echo __( 'WPBounce is a WordPress plugin that helps you minimizing the bounce rate of your landing pages', 'wp-collab' ) ?></p>
					</div><!-- .content -->
				</a>

				<a class="item" href="http://devrix.com/shop/downloads/edd-resend-receipt/" target="_blank">
					<img
						src="<?php echo WPScraper::get_instance()->get_plugin_url() . '/images/blog-thumbnail-wp-scrapper-150x150.jpg'; ?>"
						alt=""/>
					<div class="content">
						<strong><?php echo __( 'EDD Resend Receipt', 'wp-collab' ) ?></strong>
						<p class="plugin-desc"><?php echo __( 'The EDD Resend Receipt plugin allows your customers to resend their purchase receipts', 'wp-collab' ) ?></p>
					</div><!-- .content -->
				</a>
				<!-- <a href="http://devrix.com/shop/products/" target="_blank">See more</a> -->
			</div><!-- .plugins-list -->
		</aside><!-- .panel-sidebar -->

	</section><!-- .primary -->
</div><!-- .help-page -->