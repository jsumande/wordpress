			<div id="postbox-container-1" class="postbox-container wpcollab-right-sidebar-admin">
				<div class="meta-box-sortables">
					<div class="postbox">
						<h3><span><?php _e('Help Center','wp-collab')?></span></h3>
						<div class="inside">
							<div><a href="http://devrix.com/shop/downloads/wp-collab/" target="_blank" title=""><img src="<?php echo plugins_url( 'assets/img/plugin_logo.png',dirname(__FILE__));?>" /></a></div>
							<div>
								<ul>
									<li><a href="http://devrix.com/shop/downloads/wp-collab/" target="_blank" title="Documentation">Documentation</a></li>
									<li><a href="http://devrix.com/shop/contact/" target="_blank" title="Support">Support Forums</a></li>
								</ul>
							</div>
							<!-- <div>
								<a href="#" target="_blank" title=""><img src="<?php echo plugins_url( 'assets/img/banner_email.jpg',dirname(__FILE__));?>" /></a>
								<a href="#" target="_blank" title=""><img src="<?php echo plugins_url( 'assets/img/banner_wp.jpg',dirname(__FILE__));?>" /></a>
							</div> -->
							<div class="cw-admin-footer">
								<div class="left-col"><a href="http://devrix.com/" title="devrix.com" target="_blank"><img src="<?php echo plugins_url( 'assets/img/dixy.png', dirname(__FILE__));?>" /></a></div>
								<div class="right-col">Developed by <a href="http://devrix.com/" title="DevriX" target="_blank">DevriX</a> <br /> <a href="https://twitter.com/wpdevrix" target="_blank" title="Twitter">Twitter</a> | <a href="https://www.facebook.com/DevriXLtd/" target="_blank" title="Facebook">Facebook</a></div>
							</div>
						</div>
					</div> 
				</div> 
			</div> 