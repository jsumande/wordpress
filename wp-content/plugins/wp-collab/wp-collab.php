<?php
/**
 * @package   WP_Collab
 * @author    DevriX
 * @license   GPL-2.0+
 * @link      http://devrix.com
 * @copyright 2016 DevriX
 *
 * @wordpress-plugin
 * Plugin Name:       WP Collab
 * Plugin URI:        http://devrix.com/shop/downloads/wp-collab/
 * Description:       Easy project management plugin.
 * Version:           1.0.0
 * Author:            DevriX
 * Author URI:        http://devrix.com
 * Text Domain:       wp-collab-locale
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/*----------------------------------------------------------------------------*
 * Public-Facing Functionality
 *----------------------------------------------------------------------------*/


require_once( plugin_dir_path( __FILE__ ) . 'public/wp-collab.php' );

/*
 * Register hooks that are fired when the plugin is activated or deactivated.
 * When the plugin is deleted, the uninstall.php file is loaded.
 */
register_activation_hook( __FILE__, array( 'WP_Collab', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'WP_Collab', 'deactivate' ) );

add_action( 'plugins_loaded', array( 'WP_Collab', 'get_instance' ) );

/*----------------------------------------------------------------------------*
 * Dashboard and Administrative Functionality
 *----------------------------------------------------------------------------*/

/*
 * If you want to include Ajax within the dashboard, change the following
 * conditional to:
 *
 * if ( is_admin() ) {
 *   ...
 * }
 *
 * The code below is intended to to give the lightest footprint possible.
 */
if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {

	require_once( plugin_dir_path( __FILE__ ) . 'admin/wp-collab-admin.php' );
	add_action( 'plugins_loaded', array( 'WP_Collab_Admin', 'get_instance' ) );

}
